﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly
{
    public partial class startingForm : Form
    {
        MonopolyGame game;
        int editingMode;
        Bitmap bmp;
        List<Cell> cellListForSave = new List<Cell>();
        public startingForm()
        {
            InitializeComponent();
            editingMode = Constants.NoSelection;
            bmp = new Bitmap(boardPictureBox.Image);
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            game = new MonopolyGame();
            game.Start(this);

        }

        internal void HidePlayer(Player player)
        {
            //game.board.cellList[player.position].
            //     colorPanel.Controls.Clear();
        }

        internal void ShowPlayer(Player player)
        {
            //playerNameLabel.Text = player.name;
            //playerMoneyLabel.Text = player.money.ToString();
            //playerPositionLabel.Text = player.position.ToString();

            //game.board.cellList[player.position].
            //    colorPanel.Controls.Add(new Label { Text = player.name });
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void startingForm_Load(object sender, EventArgs e)
        {
            cellTypeComboBox.SelectedIndex = 0;

        }

        private void nextTurnButton_Click(object sender, EventArgs e)
        {
            game.NextTurn();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void boardPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (editingMode == Constants.UpperLeftSelection)
            {
                X1NumericUpDown.Value = e.X;
                Y1NumericUpDown.Value = e.Y;
            }
            if (editingMode == Constants.BottomRightSelection)
            {
                X2NumericUpDown.Value = e.X;
                Y2NumericUpDown.Value = e.Y;
            }


        }

        private void defineCellsButton_Click(object sender, EventArgs e)
        {
            editingMode = Constants.UpperLeftSelection;
            cellIndexNumericUpDown.Value++;
        }

        private void boardPictureBox_Click(object sender, EventArgs e)
        {
            if (editingMode == Constants.UpperLeftSelection)
            {
                editingMode = Constants.BottomRightSelection;
            }
            else
            {
                if (editingMode == Constants.BottomRightSelection)
                {
                    editingMode = Constants.NoSelection;
                }
            }


        }

        private void rentNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            rentWithColorSetNumericUpDown.Value = rentNumericUpDown.Value * 2;
        }

        private void boardPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            //int x = e.X, y = e.Y;
            //Color c = bmp.GetPixel(y, x);
            //cellColorPanel.BackColor = c;
            cellColorPanel.BackColor = bmp.GetPixel(e.X, e.Y);
        }

        private void saveCellButton_Click(object sender, EventArgs e)
        {
            Cell c = new Cell
            {
                x1 = (int)X1NumericUpDown.Value,
                y1 = (int)Y1NumericUpDown.Value,
                x2 = (int)X2NumericUpDown.Value,
                y2 = (int)Y2NumericUpDown.Value,
                index = (int)cellIndexNumericUpDown.Value,
                cellType = cellTypeComboBox.SelectedItem.ToString(), // ???
                color = cellColorPanel.BackColor,
                propertyName = "TO DO",
                buyoutPrice = (int)buyoutPriceNumericUpDown.Value,
                rent = (int)rentNumericUpDown.Value,
                rentWithColorSet = (int)rentWithColorSetNumericUpDown.Value,
                rentWith1House = (int)rentWith1HouseNumericUpDown.Value,
                rentWith2Houses = (int)rentWith2HousesNumericUpDown.Value,
                rentWith3Houses = (int)rentWith3HousesNumericUpDown.Value,
                rentWith4Houses = (int)rentWith4HousesNumericUpDown.Value,
                rentWithHotel = (int)rentWithHotelNumericUpDown.Value,
                housesCost = (int)housesCostNumericUpDown.Value,
                hotelsCost = (int)hotelsCostNumericUpDown.Value
            };
            cellListForSave.Add(c);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = cellListForSave;
            dataGridView1.Refresh();
        }

        private void saveBoardToDiskButton_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            File.WriteAllText(@"Board" + r.Next().ToString() + ".json", JsonConvert.SerializeObject(cellListForSave));
        }
    }
}
