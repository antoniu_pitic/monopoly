﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Monopoly
{
    internal class Board
    {
        //ComunityChest comunityChest = new ComunityChest();
        //Chance chance = new Chance();

        public List<Cell> cellList = new List<Cell>();

        public Board()
        {
            cellList = JsonConvert.DeserializeObject<List<Cell>>(File.ReadAllText(@"mockBoard.json"));
            //GenerateMockCells();
        }

        private void GenerateMockCells()
        {
            int X = Constants.cellColumnLeftDistance;
            int Y = Constants.cellRowTopDistance;

            X = GenerateFirstRow(X, Y);
            Y = GenerateLastColumn(X, Y);
            X = GenerateLastRow(X, Y);
            Y = GenerateFirstColumn(X, Y);

            //File.WriteAllText(@"mockBoard.json", JsonConvert.SerializeObject(cellList));
        }

        private int GenerateFirstColumn(int X, int Y)
        {
            for (int i = 30; i < 40; i++)
            {
                Cell cell = new Cell
                {
                    x1 = X, // x
                    y1 = Y, //y
                    color = Color.AliceBlue,
                    propertyName = "Proprietatea " + i,
                    buyoutPrice = 100
                };
                cellList.Add(cell);
                Y -= Constants.cellHeight + 2;
            }

            return Y;
        }

        private int GenerateLastRow(int X, int Y)
        {
            for (int i = 20; i < 30; i++)
            {
                Cell cell = new Cell
                {
                    x1 = X,
                    y1 = Y,
                    color = Color.LightSalmon,
                    propertyName = "Proprietatea " + i,
                    buyoutPrice = 100
                };
                cellList.Add(cell);
                X -= Constants.cellWidth + 2;
            }

            return X;
        }

        private int GenerateLastColumn(int X, int Y)
        {
            for (int i = 10; i < 20; i++)
            {
                Cell cell = new Cell
                {
                    x1 = X,
                    y1 = Y,
                    color = Color.LightGreen,
                    propertyName = "Proprietatea " + i,
                    buyoutPrice = 100
                };
                cellList.Add(cell);
                Y += Constants.cellHeight + 2;
            }

            return Y;
        }

        private int GenerateFirstRow(int X, int Y)
        {
            for (int i = 0; i < 10; i++)
            {
                Cell cell = new Cell
                {
                    x1 = X,
                    y1= Y,
                    color = Color.Lavender,
                    propertyName = "Proprietatea " + i,
                    buyoutPrice = 100
                };
                cellList.Add(cell);
                X += Constants.cellWidth + 2;
            }

            return X;
        }

        public void Show(Form f)
        {
            foreach (Cell cell in cellList)
            {
                cell.Show(f);
            }
            f.Refresh();
        }
    }
}