﻿namespace Monopoly
{
    partial class startingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(startingForm));
            this.boardPictureBox = new System.Windows.Forms.PictureBox();
            this.startButton = new System.Windows.Forms.Button();
            this.dicePanel = new System.Windows.Forms.Panel();
            this.diceLabel = new System.Windows.Forms.Label();
            this.playerNameLabel = new System.Windows.Forms.Label();
            this.playerMoneyLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.playerPositionLabel = new System.Windows.Forms.Label();
            this.nextTurnButton = new System.Windows.Forms.Button();
            this.defineCellsButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.X1NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.Y1NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.Y2NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.X2NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.cellIndexNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cellTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.buyoutPriceNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cellColorPanel = new System.Windows.Forms.Panel();
            this.rentNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.rentWithColorSetNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.rentWith2HousesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.rentWith1HouseNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.rentWith4HousesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.rentWith3HousesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.rentWithHotelNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.hotelsCostNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.housesCostNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.saveCellButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.saveBoardToDiskButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.boardPictureBox)).BeginInit();
            this.dicePanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.X1NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y1NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y2NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X2NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellIndexNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buyoutPriceNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.cellColorPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rentNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWithColorSetNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWith2HousesNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWith1HouseNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWith4HousesNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWith3HousesNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWithHotelNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hotelsCostNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.housesCostNumericUpDown)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // boardPictureBox
            // 
            this.boardPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.boardPictureBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.boardPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("boardPictureBox.Image")));
            this.boardPictureBox.Location = new System.Drawing.Point(24, 17);
            this.boardPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.boardPictureBox.Name = "boardPictureBox";
            this.boardPictureBox.Size = new System.Drawing.Size(1002, 1002);
            this.boardPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.boardPictureBox.TabIndex = 0;
            this.boardPictureBox.TabStop = false;
            this.boardPictureBox.Click += new System.EventHandler(this.boardPictureBox_Click);
            this.boardPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.boardPictureBox_MouseDown);
            this.boardPictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.boardPictureBox_MouseMove);
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(37, 48);
            this.startButton.Margin = new System.Windows.Forms.Padding(4);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(180, 46);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "START";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // dicePanel
            // 
            this.dicePanel.BackColor = System.Drawing.Color.White;
            this.dicePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dicePanel.Controls.Add(this.diceLabel);
            this.dicePanel.Location = new System.Drawing.Point(84, 265);
            this.dicePanel.Name = "dicePanel";
            this.dicePanel.Size = new System.Drawing.Size(96, 94);
            this.dicePanel.TabIndex = 19;
            // 
            // diceLabel
            // 
            this.diceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diceLabel.Location = new System.Drawing.Point(0, 0);
            this.diceLabel.Name = "diceLabel";
            this.diceLabel.Size = new System.Drawing.Size(94, 92);
            this.diceLabel.TabIndex = 0;
            this.diceLabel.Text = "5";
            this.diceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playerNameLabel
            // 
            this.playerNameLabel.AutoSize = true;
            this.playerNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerNameLabel.Location = new System.Drawing.Point(10, 441);
            this.playerNameLabel.Name = "playerNameLabel";
            this.playerNameLabel.Size = new System.Drawing.Size(130, 51);
            this.playerNameLabel.TabIndex = 20;
            this.playerNameLabel.Text = "name";
            // 
            // playerMoneyLabel
            // 
            this.playerMoneyLabel.AutoSize = true;
            this.playerMoneyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerMoneyLabel.Location = new System.Drawing.Point(59, 509);
            this.playerMoneyLabel.Name = "playerMoneyLabel";
            this.playerMoneyLabel.Size = new System.Drawing.Size(94, 51);
            this.playerMoneyLabel.TabIndex = 21;
            this.playerMoneyLabel.Text = "100";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 509);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 51);
            this.label1.TabIndex = 22;
            this.label1.Text = "$";
            // 
            // playerPositionLabel
            // 
            this.playerPositionLabel.AutoSize = true;
            this.playerPositionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerPositionLabel.Location = new System.Drawing.Point(15, 586);
            this.playerPositionLabel.Name = "playerPositionLabel";
            this.playerPositionLabel.Size = new System.Drawing.Size(46, 51);
            this.playerPositionLabel.TabIndex = 23;
            this.playerPositionLabel.Text = "0";
            // 
            // nextTurnButton
            // 
            this.nextTurnButton.Location = new System.Drawing.Point(37, 117);
            this.nextTurnButton.Name = "nextTurnButton";
            this.nextTurnButton.Size = new System.Drawing.Size(191, 92);
            this.nextTurnButton.TabIndex = 24;
            this.nextTurnButton.Text = "ROLL";
            this.nextTurnButton.UseVisualStyleBackColor = true;
            this.nextTurnButton.Click += new System.EventHandler(this.nextTurnButton_Click);
            // 
            // defineCellsButton
            // 
            this.defineCellsButton.Location = new System.Drawing.Point(573, 902);
            this.defineCellsButton.Name = "defineCellsButton";
            this.defineCellsButton.Size = new System.Drawing.Size(187, 55);
            this.defineCellsButton.TabIndex = 25;
            this.defineCellsButton.Text = "Define Next Cell";
            this.defineCellsButton.UseVisualStyleBackColor = true;
            this.defineCellsButton.Click += new System.EventHandler(this.defineCellsButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.startButton);
            this.groupBox1.Controls.Add(this.dicePanel);
            this.groupBox1.Controls.Add(this.nextTurnButton);
            this.groupBox1.Controls.Add(this.playerNameLabel);
            this.groupBox1.Controls.Add(this.playerPositionLabel);
            this.groupBox1.Controls.Add(this.playerMoneyLabel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(1189, 677);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(309, 358);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Questii vechi";
            // 
            // X1NumericUpDown
            // 
            this.X1NumericUpDown.Location = new System.Drawing.Point(531, 287);
            this.X1NumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.X1NumericUpDown.Name = "X1NumericUpDown";
            this.X1NumericUpDown.Size = new System.Drawing.Size(82, 31);
            this.X1NumericUpDown.TabIndex = 27;
            // 
            // Y1NumericUpDown
            // 
            this.Y1NumericUpDown.Location = new System.Drawing.Point(610, 287);
            this.Y1NumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.Y1NumericUpDown.Name = "Y1NumericUpDown";
            this.Y1NumericUpDown.Size = new System.Drawing.Size(82, 31);
            this.Y1NumericUpDown.TabIndex = 29;
            this.Y1NumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // Y2NumericUpDown
            // 
            this.Y2NumericUpDown.Location = new System.Drawing.Point(929, 865);
            this.Y2NumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.Y2NumericUpDown.Name = "Y2NumericUpDown";
            this.Y2NumericUpDown.Size = new System.Drawing.Size(82, 31);
            this.Y2NumericUpDown.TabIndex = 33;
            // 
            // X2NumericUpDown
            // 
            this.X2NumericUpDown.Location = new System.Drawing.Point(848, 865);
            this.X2NumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.X2NumericUpDown.Name = "X2NumericUpDown";
            this.X2NumericUpDown.Size = new System.Drawing.Size(82, 31);
            this.X2NumericUpDown.TabIndex = 31;
            // 
            // cellIndexNumericUpDown
            // 
            this.cellIndexNumericUpDown.Location = new System.Drawing.Point(99, 3);
            this.cellIndexNumericUpDown.Name = "cellIndexNumericUpDown";
            this.cellIndexNumericUpDown.Size = new System.Drawing.Size(76, 31);
            this.cellIndexNumericUpDown.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 25);
            this.label6.TabIndex = 36;
            this.label6.Text = "Cell ID";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 25);
            this.label7.TabIndex = 37;
            this.label7.Text = "Cell Type";
            // 
            // cellTypeComboBox
            // 
            this.cellTypeComboBox.FormattingEnabled = true;
            this.cellTypeComboBox.Items.AddRange(new object[] {
            "Standard property",
            "Community Chest",
            "Chance",
            "Start",
            "Jail",
            "Free Parking"});
            this.cellTypeComboBox.Location = new System.Drawing.Point(123, 42);
            this.cellTypeComboBox.Name = "cellTypeComboBox";
            this.cellTypeComboBox.Size = new System.Drawing.Size(168, 33);
            this.cellTypeComboBox.TabIndex = 38;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(132, 25);
            this.label8.TabIndex = 39;
            this.label8.Text = "Buyout price";
            // 
            // buyoutPriceNumericUpDown
            // 
            this.buyoutPriceNumericUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.buyoutPriceNumericUpDown.Location = new System.Drawing.Point(152, 79);
            this.buyoutPriceNumericUpDown.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.buyoutPriceNumericUpDown.Minimum = new decimal(new int[] {
            140,
            0,
            0,
            0});
            this.buyoutPriceNumericUpDown.Name = "buyoutPriceNumericUpDown";
            this.buyoutPriceNumericUpDown.Size = new System.Drawing.Size(94, 31);
            this.buyoutPriceNumericUpDown.TabIndex = 41;
            this.buyoutPriceNumericUpDown.Value = new decimal(new int[] {
            140,
            0,
            0,
            0});
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(533, 288);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(469, 608);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            // 
            // cellColorPanel
            // 
            this.cellColorPanel.Controls.Add(this.buyoutPriceNumericUpDown);
            this.cellColorPanel.Controls.Add(this.cellTypeComboBox);
            this.cellColorPanel.Controls.Add(this.label8);
            this.cellColorPanel.Controls.Add(this.label7);
            this.cellColorPanel.Controls.Add(this.label6);
            this.cellColorPanel.Controls.Add(this.cellIndexNumericUpDown);
            this.cellColorPanel.Location = new System.Drawing.Point(605, 326);
            this.cellColorPanel.Name = "cellColorPanel";
            this.cellColorPanel.Size = new System.Drawing.Size(329, 127);
            this.cellColorPanel.TabIndex = 42;
            // 
            // rentNumericUpDown
            // 
            this.rentNumericUpDown.Location = new System.Drawing.Point(837, 460);
            this.rentNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.rentNumericUpDown.Name = "rentNumericUpDown";
            this.rentNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.rentNumericUpDown.TabIndex = 43;
            this.rentNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.rentNumericUpDown.ValueChanged += new System.EventHandler(this.rentNumericUpDown_ValueChanged);
            // 
            // rentWithColorSetNumericUpDown
            // 
            this.rentWithColorSetNumericUpDown.Enabled = false;
            this.rentWithColorSetNumericUpDown.Location = new System.Drawing.Point(837, 497);
            this.rentWithColorSetNumericUpDown.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.rentWithColorSetNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.rentWithColorSetNumericUpDown.Name = "rentWithColorSetNumericUpDown";
            this.rentWithColorSetNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.rentWithColorSetNumericUpDown.TabIndex = 44;
            this.rentWithColorSetNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // rentWith2HousesNumericUpDown
            // 
            this.rentWith2HousesNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.rentWith2HousesNumericUpDown.Location = new System.Drawing.Point(837, 574);
            this.rentWith2HousesNumericUpDown.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.rentWith2HousesNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.rentWith2HousesNumericUpDown.Name = "rentWith2HousesNumericUpDown";
            this.rentWith2HousesNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.rentWith2HousesNumericUpDown.TabIndex = 46;
            this.rentWith2HousesNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // rentWith1HouseNumericUpDown
            // 
            this.rentWith1HouseNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.rentWith1HouseNumericUpDown.Location = new System.Drawing.Point(837, 537);
            this.rentWith1HouseNumericUpDown.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.rentWith1HouseNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.rentWith1HouseNumericUpDown.Name = "rentWith1HouseNumericUpDown";
            this.rentWith1HouseNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.rentWith1HouseNumericUpDown.TabIndex = 45;
            this.rentWith1HouseNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // rentWith4HousesNumericUpDown
            // 
            this.rentWith4HousesNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.rentWith4HousesNumericUpDown.Location = new System.Drawing.Point(837, 653);
            this.rentWith4HousesNumericUpDown.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.rentWith4HousesNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.rentWith4HousesNumericUpDown.Name = "rentWith4HousesNumericUpDown";
            this.rentWith4HousesNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.rentWith4HousesNumericUpDown.TabIndex = 48;
            this.rentWith4HousesNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // rentWith3HousesNumericUpDown
            // 
            this.rentWith3HousesNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.rentWith3HousesNumericUpDown.Location = new System.Drawing.Point(837, 612);
            this.rentWith3HousesNumericUpDown.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.rentWith3HousesNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.rentWith3HousesNumericUpDown.Name = "rentWith3HousesNumericUpDown";
            this.rentWith3HousesNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.rentWith3HousesNumericUpDown.TabIndex = 47;
            this.rentWith3HousesNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // rentWithHotelNumericUpDown
            // 
            this.rentWithHotelNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.rentWithHotelNumericUpDown.Location = new System.Drawing.Point(837, 691);
            this.rentWithHotelNumericUpDown.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.rentWithHotelNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.rentWithHotelNumericUpDown.Name = "rentWithHotelNumericUpDown";
            this.rentWithHotelNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.rentWithHotelNumericUpDown.TabIndex = 49;
            this.rentWithHotelNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // hotelsCostNumericUpDown
            // 
            this.hotelsCostNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.hotelsCostNumericUpDown.Location = new System.Drawing.Point(783, 791);
            this.hotelsCostNumericUpDown.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.hotelsCostNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.hotelsCostNumericUpDown.Name = "hotelsCostNumericUpDown";
            this.hotelsCostNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.hotelsCostNumericUpDown.TabIndex = 51;
            this.hotelsCostNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // housesCostNumericUpDown
            // 
            this.housesCostNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.housesCostNumericUpDown.Location = new System.Drawing.Point(783, 753);
            this.housesCostNumericUpDown.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.housesCostNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.housesCostNumericUpDown.Name = "housesCostNumericUpDown";
            this.housesCostNumericUpDown.Size = new System.Drawing.Size(86, 31);
            this.housesCostNumericUpDown.TabIndex = 50;
            this.housesCostNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // saveCellButton
            // 
            this.saveCellButton.Location = new System.Drawing.Point(766, 902);
            this.saveCellButton.Name = "saveCellButton";
            this.saveCellButton.Size = new System.Drawing.Size(187, 55);
            this.saveCellButton.TabIndex = 52;
            this.saveCellButton.Text = "Save Cell";
            this.saveCellButton.UseVisualStyleBackColor = true;
            this.saveCellButton.Click += new System.EventHandler(this.saveCellButton_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(1057, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(631, 569);
            this.panel1.TabIndex = 53;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(17, 22);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 82;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(581, 450);
            this.dataGridView1.TabIndex = 0;
            // 
            // saveBoardToDiskButton
            // 
            this.saveBoardToDiskButton.Location = new System.Drawing.Point(664, 963);
            this.saveBoardToDiskButton.Name = "saveBoardToDiskButton";
            this.saveBoardToDiskButton.Size = new System.Drawing.Size(187, 55);
            this.saveBoardToDiskButton.TabIndex = 54;
            this.saveBoardToDiskButton.Text = "Save To Disk";
            this.saveBoardToDiskButton.UseVisualStyleBackColor = true;
            this.saveBoardToDiskButton.Click += new System.EventHandler(this.saveBoardToDiskButton_Click);
            // 
            // startingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1700, 1061);
            this.Controls.Add(this.saveBoardToDiskButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.saveCellButton);
            this.Controls.Add(this.hotelsCostNumericUpDown);
            this.Controls.Add(this.housesCostNumericUpDown);
            this.Controls.Add(this.rentWithHotelNumericUpDown);
            this.Controls.Add(this.rentWith4HousesNumericUpDown);
            this.Controls.Add(this.rentWith3HousesNumericUpDown);
            this.Controls.Add(this.rentWith2HousesNumericUpDown);
            this.Controls.Add(this.rentWith1HouseNumericUpDown);
            this.Controls.Add(this.rentWithColorSetNumericUpDown);
            this.Controls.Add(this.rentNumericUpDown);
            this.Controls.Add(this.defineCellsButton);
            this.Controls.Add(this.cellColorPanel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.X1NumericUpDown);
            this.Controls.Add(this.Y1NumericUpDown);
            this.Controls.Add(this.X2NumericUpDown);
            this.Controls.Add(this.Y2NumericUpDown);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.boardPictureBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "startingForm";
            this.Text = "MONOPOLY";
            this.Load += new System.EventHandler(this.startingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.boardPictureBox)).EndInit();
            this.dicePanel.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.X1NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y1NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y2NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.X2NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellIndexNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buyoutPriceNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.cellColorPanel.ResumeLayout(false);
            this.cellColorPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rentNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWithColorSetNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWith2HousesNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWith1HouseNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWith4HousesNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWith3HousesNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentWithHotelNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hotelsCostNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.housesCostNumericUpDown)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox boardPictureBox;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Panel dicePanel;
        private System.Windows.Forms.Label diceLabel;
        private System.Windows.Forms.Label playerNameLabel;
        private System.Windows.Forms.Label playerMoneyLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label playerPositionLabel;
        private System.Windows.Forms.Button nextTurnButton;
        private System.Windows.Forms.Button defineCellsButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown X1NumericUpDown;
        private System.Windows.Forms.NumericUpDown Y1NumericUpDown;
        private System.Windows.Forms.NumericUpDown Y2NumericUpDown;
        private System.Windows.Forms.NumericUpDown X2NumericUpDown;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown cellIndexNumericUpDown;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown buyoutPriceNumericUpDown;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cellTypeComboBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel cellColorPanel;
        private System.Windows.Forms.NumericUpDown rentNumericUpDown;
        private System.Windows.Forms.NumericUpDown rentWithColorSetNumericUpDown;
        private System.Windows.Forms.NumericUpDown rentWith2HousesNumericUpDown;
        private System.Windows.Forms.NumericUpDown rentWith1HouseNumericUpDown;
        private System.Windows.Forms.NumericUpDown rentWith4HousesNumericUpDown;
        private System.Windows.Forms.NumericUpDown rentWith3HousesNumericUpDown;
        private System.Windows.Forms.NumericUpDown rentWithHotelNumericUpDown;
        private System.Windows.Forms.NumericUpDown hotelsCostNumericUpDown;
        private System.Windows.Forms.NumericUpDown housesCostNumericUpDown;
        private System.Windows.Forms.Button saveCellButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button saveBoardToDiskButton;
    }
}

