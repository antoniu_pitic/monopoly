﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly
{
    class LINQTestObjects
    {
        List<Cell> cellList = new List<Cell>();

        public LINQTestObjects()
        {
            cellList = JsonConvert.DeserializeObject<List<Cell>>(File.ReadAllText(@"board.json"));
        }

        internal string Show()
        {
            string output = string.Empty;
            foreach(Cell x in cellList)
            {
                output += x.propertyName + "-" + x.buyoutPrice + "  ";
            }
            return output;
        }

        public string Maxim()
        {
            string mx = "";

            foreach (var x in cellList)
            {
                if(x.propertyName.CompareTo(mx) > 0)
                {
                    mx = x.propertyName;
                }

            }

            return mx;
        }

        public string f(Cell x)
        {
            return x.propertyName;
        }
        public string MaximLINQ()
        {
            //return cellList.Max(f);
            return cellList.Max(x => x.propertyName);

        }

        private int f(Cell x, Cell y)
        {
            return x.propertyName.CompareTo(y.propertyName);
        }

        public void Sort()
        {
            cellList.Sort( (x,y) => x.propertyName.CompareTo(y.propertyName) ); // vezi functia de mai sus
        }
      public void SortByBuyout()
        {
            cellList.Sort( (x,y) => x.buyoutPrice - y.buyoutPrice ); 
        }

        public void RandomizeList() {
            Random r = new Random();
            cellList.Sort((x, y) => r.Next(-1,2));
        }
        private bool g(Cell x)
        {
            return x.color == Color.Red;
        }
        internal string StringCuAleaRosii()
        {
            //List<Cell> aleaRosii = cellList.Where( x => x.color == Color.Red).ToList() ;

            List<Cell> aleaRosii = (
                                    from x in cellList
                                    where x.color == Color.Red
                                    select x).ToList();

            string output = string.Empty;
            foreach (Cell x in aleaRosii)
            {
                output += " " + x.propertyName + "-" + x.color;
            }
            return output;
        }

        public string StringCuAleaScumpe(int price)
        {

            string output = string.Empty;
            foreach (Cell x in cellList.Where(x => x.buyoutPrice >= price))
            {
                output += " " + x.propertyName + "-" + x.buyoutPrice;
            }
            return output;
        }

        public int SumOfBuyoutPrices()
        {
            return cellList.Sum(x => x.buyoutPrice);
        }



    }
}

