﻿namespace Monopoly
{
    partial class LINQTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.testButton = new System.Windows.Forms.Button();
            this.valuesListBox = new System.Windows.Forms.ListBox();
            this.cellTestsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // testButton
            // 
            this.testButton.Location = new System.Drawing.Point(12, 46);
            this.testButton.Name = "testButton";
            this.testButton.Size = new System.Drawing.Size(128, 47);
            this.testButton.TabIndex = 0;
            this.testButton.Text = "Push Me";
            this.testButton.UseVisualStyleBackColor = true;
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // valuesListBox
            // 
            this.valuesListBox.FormattingEnabled = true;
            this.valuesListBox.ItemHeight = 25;
            this.valuesListBox.Location = new System.Drawing.Point(36, 137);
            this.valuesListBox.Name = "valuesListBox";
            this.valuesListBox.Size = new System.Drawing.Size(1104, 454);
            this.valuesListBox.TabIndex = 1;
            // 
            // cellTestsButton
            // 
            this.cellTestsButton.Location = new System.Drawing.Point(192, 46);
            this.cellTestsButton.Name = "cellTestsButton";
            this.cellTestsButton.Size = new System.Drawing.Size(128, 62);
            this.cellTestsButton.TabIndex = 2;
            this.cellTestsButton.Text = "Cell Tests";
            this.cellTestsButton.UseVisualStyleBackColor = true;
            this.cellTestsButton.Click += new System.EventHandler(this.cellTestsButton_Click);
            // 
            // LINQTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1175, 1072);
            this.Controls.Add(this.cellTestsButton);
            this.Controls.Add(this.valuesListBox);
            this.Controls.Add(this.testButton);
            this.Name = "LINQTestForm";
            this.Text = "LINQTestForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button testButton;
        private System.Windows.Forms.ListBox valuesListBox;
        private System.Windows.Forms.Button cellTestsButton;
    }
}